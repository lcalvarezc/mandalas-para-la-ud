# Mandalas para la UD

Proyecto por medio de Krita que propone crear mándalas para los estudiantes de primer semestre de ingeniería de sistemas de la universidad distrital, este se hizo con las 3 primeras como ejemplo, para las ultimas tres ser diseñadas deacuerdo a los gustos de los estudiantes, asi como creadno frases que estos encontraban inspiradoras.


## Nombre
Mandalas para la UD(ESPECIFICAMENTE ESTUDIANTES DE PRIMER SEMESTRE de ingenieria de sistemas).

## Descripcion
Grupo de 6 mandalas y 3 tres frases motivadoras, inspirada en los gustos de los estudiantes, para incentivar el que realizen meditacion por medio de mandalas.

## Contenidos y orden reocomendado
    Mandala 1
    Mandala 2
    Frase 1
    Mandala 3
    Mandala 4
    Frase 3
    Mandala 5
    Mandala 6
    Frase 2
    Presentacion del proyecto



## Authors and acknowledgment
Laura Carina Alvarez
